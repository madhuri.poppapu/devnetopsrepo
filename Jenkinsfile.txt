pipeline {
    agent any
    stages {
        stage('Clean and prep workspace') {
            steps {
                git 'https://gitlab.com/madhuri.poppapu/devnetopsrepo.git'
            }
        }
        stage('Staging') {
            steps {
                sh 'ansible-playbook playbook.yml'
            }
        }
    }
}
